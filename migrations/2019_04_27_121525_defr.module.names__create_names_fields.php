<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;
use Anomaly\UsersModule\User\UserModel;
use Defr\NamesModule\FirstName\FirstNameModel;
use Defr\NamesModule\LastName\LastNameModel;
use Defr\NamesModule\MiddleName\MiddleNameModel;

class DefrModuleNamesCreateNamesFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        'name'        => 'anomaly.field_type.text',
        'first_name'  => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related' => FirstNameModel::class,
            ],
        ],
        'last_name'   => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related' => LastNameModel::class,
            ],
        ],
        'middle_name' => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related' => MiddleNameModel::class,
            ],
        ],
        'users'        => [
            'type'   => 'anomaly.field_type.multiple',
            'config' => [
                'related' => UserModel::class,
            ],
        ],
    ];

}
