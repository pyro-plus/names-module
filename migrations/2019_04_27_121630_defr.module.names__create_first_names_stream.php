<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrModuleNamesCreateFirstNamesStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug'         => 'first_names',
        'title_column' => 'name',
        'searchable'   => true,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name' => [
            'required' => true,
        ],
    ];

}
