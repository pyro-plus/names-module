<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrModuleNamesCreateLastNamesStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug'         => 'last_names',
        'title_column' => 'name',
        'searchable'   => true,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name' => [
            'required' => true,
        ],
    ];

}
