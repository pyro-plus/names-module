<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrModuleNamesCreateMiddleNamesStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug'         => 'middle_names',
        'title_column' => 'name',
        'searchable'   => true,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name' => [
            'required' => true,
        ],
    ];

}
