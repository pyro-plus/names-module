<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrModuleNamesCreateFullNamesStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'full_names',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'first_name' => [
            'required' => true,
        ],
        'last_name'  => [
            'required' => true,
        ],
        'middle_name',
        'users',
    ];

}
