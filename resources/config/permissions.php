<?php

return [
    'first_names' => [
        'read',
        'write',
        'delete',
    ],
    'last_names' => [
        'read',
        'write',
        'delete',
    ],
    'middle_names' => [
        'read',
        'write',
        'delete',
    ],
    'full_names' => [
        'read',
        'write',
        'delete',
    ],
];
