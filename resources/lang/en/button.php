<?php

return [
    'new_first_name' => 'New First name',
    'new_last_name' => 'New Last name',
    'new_middle_name' => 'New Middle name',
    'new_full_name' => 'New Full name',
];
