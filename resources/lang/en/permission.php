<?php

return [
    'first_names' => [
        'name'   => 'First names',
        'option' => [
            'read'   => 'Can read first names?',
            'write'  => 'Can create/edit first names?',
            'delete' => 'Can delete first names?',
        ],
    ],
    'last_names' => [
        'name'   => 'Last names',
        'option' => [
            'read'   => 'Can read last names?',
            'write'  => 'Can create/edit last names?',
            'delete' => 'Can delete last names?',
        ],
    ],
    'middle_names' => [
        'name'   => 'Middle names',
        'option' => [
            'read'   => 'Can read middle names?',
            'write'  => 'Can create/edit middle names?',
            'delete' => 'Can delete middle names?',
        ],
    ],
    'full_names' => [
        'name'   => 'Full names',
        'option' => [
            'read'   => 'Can read full names?',
            'write'  => 'Can create/edit full names?',
            'delete' => 'Can delete full names?',
        ],
    ],
];
