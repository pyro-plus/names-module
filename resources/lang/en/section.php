<?php

return [
    'first_names' => [
        'title' => 'First names',
    ],
    'last_names' => [
        'title' => 'Last names',
    ],
    'middle_names' => [
        'title' => 'Middle names',
    ],
    'full_names' => [
        'title' => 'Full names',
    ],
];
