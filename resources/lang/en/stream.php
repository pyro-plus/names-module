<?php

return [
    'first_names' => [
        'name' => 'First names',
    ],
    'last_names' => [
        'name' => 'Last names',
    ],
    'middle_names' => [
        'name' => 'Middle names',
    ],
    'full_names' => [
        'name' => 'Full names',
    ],
];
