<?php namespace Defr\NamesModule\FirstName\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface FirstNameRepositoryInterface extends EntryRepositoryInterface
{

}
