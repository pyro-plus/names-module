<?php namespace Defr\NamesModule\FirstName;

use Defr\NamesModule\FirstName\Contract\FirstNameRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class FirstNameRepository extends EntryRepository implements FirstNameRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var FirstNameModel
     */
    protected $model;

    /**
     * Create a new FirstNameRepository instance.
     *
     * @param FirstNameModel $model
     */
    public function __construct(FirstNameModel $model)
    {
        $this->model = $model;
    }
}
