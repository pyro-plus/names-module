<?php namespace Defr\NamesModule\FullName\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface FullNameRepositoryInterface extends EntryRepositoryInterface
{

}
