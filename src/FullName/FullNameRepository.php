<?php namespace Defr\NamesModule\FullName;

use Defr\NamesModule\FullName\Contract\FullNameRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class FullNameRepository extends EntryRepository implements FullNameRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var FullNameModel
     */
    protected $model;

    /**
     * Create a new FullNameRepository instance.
     *
     * @param FullNameModel $model
     */
    public function __construct(FullNameModel $model)
    {
        $this->model = $model;
    }
}
