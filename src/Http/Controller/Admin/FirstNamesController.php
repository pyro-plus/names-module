<?php namespace Defr\NamesModule\Http\Controller\Admin;

use Defr\NamesModule\FirstName\Form\FirstNameFormBuilder;
use Defr\NamesModule\FirstName\Table\FirstNameTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class FirstNamesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param FirstNameTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(FirstNameTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param FirstNameFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(FirstNameFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param FirstNameFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(FirstNameFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
