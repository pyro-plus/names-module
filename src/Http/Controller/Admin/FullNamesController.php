<?php namespace Defr\NamesModule\Http\Controller\Admin;

use Defr\NamesModule\FullName\Form\FullNameFormBuilder;
use Defr\NamesModule\FullName\Table\FullNameTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class FullNamesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param FullNameTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(FullNameTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param FullNameFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(FullNameFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param FullNameFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(FullNameFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
