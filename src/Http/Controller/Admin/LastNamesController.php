<?php namespace Defr\NamesModule\Http\Controller\Admin;

use Defr\NamesModule\LastName\Form\LastNameFormBuilder;
use Defr\NamesModule\LastName\Table\LastNameTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class LastNamesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param LastNameTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(LastNameTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param LastNameFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(LastNameFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param LastNameFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(LastNameFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
