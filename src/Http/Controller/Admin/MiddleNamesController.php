<?php namespace Defr\NamesModule\Http\Controller\Admin;

use Defr\NamesModule\MiddleName\Form\MiddleNameFormBuilder;
use Defr\NamesModule\MiddleName\Table\MiddleNameTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class MiddleNamesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param MiddleNameTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(MiddleNameTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param MiddleNameFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(MiddleNameFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param MiddleNameFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(MiddleNameFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
