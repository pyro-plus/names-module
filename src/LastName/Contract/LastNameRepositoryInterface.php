<?php namespace Defr\NamesModule\LastName\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface LastNameRepositoryInterface extends EntryRepositoryInterface
{

}
