<?php namespace Defr\NamesModule\LastName;

use Defr\NamesModule\LastName\Contract\LastNameRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class LastNameRepository extends EntryRepository implements LastNameRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var LastNameModel
     */
    protected $model;

    /**
     * Create a new LastNameRepository instance.
     *
     * @param LastNameModel $model
     */
    public function __construct(LastNameModel $model)
    {
        $this->model = $model;
    }
}
