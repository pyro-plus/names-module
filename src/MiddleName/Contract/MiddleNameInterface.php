<?php namespace Defr\NamesModule\MiddleName\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

interface MiddleNameInterface extends EntryInterface
{

}
