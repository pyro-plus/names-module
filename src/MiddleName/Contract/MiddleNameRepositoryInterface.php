<?php namespace Defr\NamesModule\MiddleName\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface MiddleNameRepositoryInterface extends EntryRepositoryInterface
{

}
