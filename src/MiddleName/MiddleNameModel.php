<?php namespace Defr\NamesModule\MiddleName;

use Defr\NamesModule\MiddleName\Contract\MiddleNameInterface;
use Anomaly\Streams\Platform\Model\Names\NamesMiddleNamesEntryModel;

class MiddleNameModel extends NamesMiddleNamesEntryModel implements MiddleNameInterface
{

}
