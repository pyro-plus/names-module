<?php namespace Defr\NamesModule\MiddleName;

use Defr\NamesModule\MiddleName\Contract\MiddleNameRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class MiddleNameRepository extends EntryRepository implements MiddleNameRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var MiddleNameModel
     */
    protected $model;

    /**
     * Create a new MiddleNameRepository instance.
     *
     * @param MiddleNameModel $model
     */
    public function __construct(MiddleNameModel $model)
    {
        $this->model = $model;
    }
}
