<?php namespace Defr\NamesModule;

use Anomaly\Streams\Platform\Addon\Module\Module;

class NamesModule extends Module
{

    /**
     * The navigation display flag.
     *
     * @var bool
     */
    protected $navigation = true;

    /**
     * The addon icon.
     *
     * @var string
     */
    protected $icon = 'fa fa-puzzle-piece';

    /**
     * The module sections.
     *
     * @var array
     */
    protected $sections = [
        'first_names' => [
            'buttons' => [
                'new_first_name',
            ],
        ],
        'last_names' => [
            'buttons' => [
                'new_last_name',
            ],
        ],
        'middle_names' => [
            'buttons' => [
                'new_middle_name',
            ],
        ],
        'full_names' => [
            'buttons' => [
                'new_full_name',
            ],
        ],
    ];

}
