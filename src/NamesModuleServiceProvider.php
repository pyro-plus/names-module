<?php namespace Defr\NamesModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Defr\NamesModule\FullName\Contract\FullNameRepositoryInterface;
use Defr\NamesModule\FullName\FullNameRepository;
use Anomaly\Streams\Platform\Model\Names\NamesFullNamesEntryModel;
use Defr\NamesModule\FullName\FullNameModel;
use Defr\NamesModule\MiddleName\Contract\MiddleNameRepositoryInterface;
use Defr\NamesModule\MiddleName\MiddleNameRepository;
use Anomaly\Streams\Platform\Model\Names\NamesMiddleNamesEntryModel;
use Defr\NamesModule\MiddleName\MiddleNameModel;
use Defr\NamesModule\LastName\Contract\LastNameRepositoryInterface;
use Defr\NamesModule\LastName\LastNameRepository;
use Anomaly\Streams\Platform\Model\Names\NamesLastNamesEntryModel;
use Defr\NamesModule\LastName\LastNameModel;
use Defr\NamesModule\FirstName\Contract\FirstNameRepositoryInterface;
use Defr\NamesModule\FirstName\FirstNameRepository;
use Anomaly\Streams\Platform\Model\Names\NamesFirstNamesEntryModel;
use Defr\NamesModule\FirstName\FirstNameModel;
use Illuminate\Routing\Router;

class NamesModuleServiceProvider extends AddonServiceProvider
{

    /**
     * Additional addon plugins.
     *
     * @type array|null
     */
    protected $plugins = [];

    /**
     * The addon Artisan commands.
     *
     * @type array|null
     */
    protected $commands = [];

    /**
     * The addon's scheduled commands.
     *
     * @type array|null
     */
    protected $schedules = [];

    /**
     * The addon API routes.
     *
     * @type array|null
     */
    protected $api = [];

    /**
     * The addon routes.
     *
     * @type array|null
     */
    protected $routes = [
        'admin/names/full_names'           => 'Defr\NamesModule\Http\Controller\Admin\FullNamesController@index',
        'admin/names/full_names/create'    => 'Defr\NamesModule\Http\Controller\Admin\FullNamesController@create',
        'admin/names/full_names/edit/{id}' => 'Defr\NamesModule\Http\Controller\Admin\FullNamesController@edit',
        'admin/names/middle_names'           => 'Defr\NamesModule\Http\Controller\Admin\MiddleNamesController@index',
        'admin/names/middle_names/create'    => 'Defr\NamesModule\Http\Controller\Admin\MiddleNamesController@create',
        'admin/names/middle_names/edit/{id}' => 'Defr\NamesModule\Http\Controller\Admin\MiddleNamesController@edit',
        'admin/names/last_names'           => 'Defr\NamesModule\Http\Controller\Admin\LastNamesController@index',
        'admin/names/last_names/create'    => 'Defr\NamesModule\Http\Controller\Admin\LastNamesController@create',
        'admin/names/last_names/edit/{id}' => 'Defr\NamesModule\Http\Controller\Admin\LastNamesController@edit',
        'admin/names'           => 'Defr\NamesModule\Http\Controller\Admin\FirstNamesController@index',
        'admin/names/create'    => 'Defr\NamesModule\Http\Controller\Admin\FirstNamesController@create',
        'admin/names/edit/{id}' => 'Defr\NamesModule\Http\Controller\Admin\FirstNamesController@edit',
    ];

    /**
     * The addon middleware.
     *
     * @type array|null
     */
    protected $middleware = [
        //Defr\NamesModule\Http\Middleware\ExampleMiddleware::class
    ];

    /**
     * Addon group middleware.
     *
     * @var array
     */
    protected $groupMiddleware = [
        //'web' => [
        //    Defr\NamesModule\Http\Middleware\ExampleMiddleware::class,
        //],
    ];

    /**
     * Addon route middleware.
     *
     * @type array|null
     */
    protected $routeMiddleware = [];

    /**
     * The addon event listeners.
     *
     * @type array|null
     */
    protected $listeners = [
        //Defr\NamesModule\Event\ExampleEvent::class => [
        //    Defr\NamesModule\Listener\ExampleListener::class,
        //],
    ];

    /**
     * The addon alias bindings.
     *
     * @type array|null
     */
    protected $aliases = [
        //'Example' => Defr\NamesModule\Example::class
    ];

    /**
     * The addon class bindings.
     *
     * @type array|null
     */
    protected $bindings = [
        NamesFullNamesEntryModel::class => FullNameModel::class,
        NamesMiddleNamesEntryModel::class => MiddleNameModel::class,
        NamesLastNamesEntryModel::class => LastNameModel::class,
        NamesFirstNamesEntryModel::class => FirstNameModel::class,
    ];

    /**
     * The addon singleton bindings.
     *
     * @type array|null
     */
    protected $singletons = [
        FullNameRepositoryInterface::class => FullNameRepository::class,
        MiddleNameRepositoryInterface::class => MiddleNameRepository::class,
        LastNameRepositoryInterface::class => LastNameRepository::class,
        FirstNameRepositoryInterface::class => FirstNameRepository::class,
    ];

    /**
     * Additional service providers.
     *
     * @type array|null
     */
    protected $providers = [
        //\ExamplePackage\Provider\ExampleProvider::class
    ];

    /**
     * The addon view overrides.
     *
     * @type array|null
     */
    protected $overrides = [
        //'streams::errors/404' => 'module::errors/404',
        //'streams::errors/500' => 'module::errors/500',
    ];

    /**
     * The addon mobile-only view overrides.
     *
     * @type array|null
     */
    protected $mobile = [
        //'streams::errors/404' => 'module::mobile/errors/404',
        //'streams::errors/500' => 'module::mobile/errors/500',
    ];

    /**
     * Register the addon.
     */
    public function register()
    {
        // Run extra pre-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Boot the addon.
     */
    public function boot()
    {
        // Run extra post-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Map additional addon routes.
     *
     * @param Router $router
     */
    public function map(Router $router)
    {
        // Register dynamic routes here for example.
        // Use method injection or commands to bring in services.
    }

}
